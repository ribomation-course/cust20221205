# Installation Instructions
To perform the programming exercises, you need:

* **C++ compiler**, supporting C++ 17 or even better C++20, _to compile your code_
* **C++ IDE**, _to write your code_
* **GIT client**, _to get the solutions from this repo_

There are many ways to write and compile C++ programs both on Windows,
Linux and Mac. The instructions below provide you with some alternatives.

# Pure Windows Setup

## Visual Studio Community
Download and install from

* https://visualstudio.microsoft.com/vs/community/

This will provide you with a complete set of tools to use. 
Here is a video of how to get started

* [Create Your First C++ Project Using Microsoft Visual Studio 2022](https://youtu.be/wZZW0p9iSy4)


## CLion + Build Tools for Visual Studio 2022

Download and install

* https://www.jetbrains.com/clion/
* https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2022

Follow the instructions at

* https://www.jetbrains.com/help/clion/quick-tutorial-on-configuring-clion-on-windows.html#MSVC


# Mixed Windows/Ubuntu Setup

## WSL
If you are using Windows 11 (or 10), it supports WSL 2, which stands for
Windows Subsystem for Linux, version 2. It's an excellent development
environment and provides the best of Windows and Linux together.
First setup WSL

* https://learn.microsoft.com/en-us/windows/wsl/install

## Ubuntu @ MS Store
Then open _Microsoft Store_ and search for "Ubuntu" and choose
Ubuntu 22 (_Ubuntu 22.04.1 LTS_) and install it. It's as simple as that.

## CLion
My favorite C++ IDE is CLion, and it works very nicely with WSL; 
i.e., you install _CLion on Windows_ and uses the compiler resources within WSL. 
First install it from

* https://www.jetbrains.com/clion/

Then, just follow the instructions at
* [How to Use WSL Development Environment in CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
* [Using WSL toolchains in CLion on Windows (YouTube)](https://youtu.be/xnwoCuHeHuY)

This is also my setup I use all the time and will be using during the course.

## GCC Compiler and Tools
Within an Ubuntu terminal window type the following command to install the
compiler and other tools.

    sudo apt install g++ gdb make cmake valgrind

_N.B._ when you run a `sudo` command it prompts you for the password, you use
to log on to Ubuntu. If you're running another OS, amend the installation command
accordingly.

Verify the version

    g++ --version


## How to change the default GCC version

When you install a particular version of GCC it might be that the linux
system has another (lower) version as its standard. 

First you have to install the newer compiler, that means, you now have at
least two versions. For example, try

    sudo apt install g++-12

To change the default you use the `update-alternatives` command 
(*one long command-line below*)

    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-12 90 --slave /usr/bin/g++ g++ /usr/bin/g++-12 --slave /usr/bin/gcov gcov /usr/bin/gcov-12

Verify that GCC version 12, now is the default

    g++ --version


# Pure Linux Setup
If you already have a computer running Linux, you probably are good to go.
If it's not Ubuntu you might have to adjust the installation commands.


# Write, compile and run
To verify you have it all set up and are ready to go. 
Write the following C++ program, compile it within you IDE and run it.

    #include <iostream>
    using std::cout;

    int main() {
        cout << "Hello from my 1st C++ program\n";
    }

