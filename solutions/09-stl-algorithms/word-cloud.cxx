#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <functional>
#include <filesystem>
#include <utility>
#include <random>
#include <cctype>

using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;

using WordFreqTbl = unordered_map<string, unsigned>;
using WordFreq = pair<string, unsigned>;

auto load(istream& infile, unsigned minWordSize) -> WordFreqTbl;
auto sorted(WordFreqTbl& frequencies, unsigned maxNumWords) -> vector<WordFreq>;
auto mkTags(vector<WordFreq> words, float minFont, float maxFont) -> vector<string>;
void store(string filename, const vector<string>& tags);

random_device dev_random;
default_random_engine r{dev_random()};

int main(int argc, char** argv) {
    auto filename = fs::path{(argc > 1) ? argv[1] : "../musketeers.txt"s};
    const auto minWordSize = 5U;
    const auto maxNumWords = 100U;
    const auto minFontSize = 15.F;
    const auto maxFontSize = 150.F;

    auto infile = ifstream{filename};
    if (!infile) throw invalid_argument{"cannot open "s + filename.string()};

    auto frequencies = load(infile, minWordSize);
    auto words = sorted(frequencies, maxNumWords);
    auto tags = mkTags(words, minFontSize, maxFontSize);

    const auto outFilename = filename.replace_extension(".html");
    store(outFilename, tags);
    cout << "written " << outFilename << endl;
}

auto strip(string line) {
    transform(begin(line), end(line), begin(line), [](auto ch) {
        return isalpha(ch) ? tolower(ch) : ' ';
    });
    return line;
}

void accumulate(string const& line,
                function<bool(string const& word)> shouldAdd,
                function<void(string const& word)> processWord) {
    auto buf = istringstream{line};
    for (string word; buf >> word;) {
        if (shouldAdd(word)) processWord(word);
    }
}

auto load(istream& infile, unsigned minLen) -> WordFreqTbl {
    auto frequencies = WordFreqTbl{};
    for (auto line = ""s; getline(infile, line);) {
        accumulate(strip(std::move(line)),
                   [minLen](string const& word) { return word.size() >= minLen; },
                   [&frequencies](string const& word) { ++frequencies[word]; });
    }
    return frequencies;
}

auto sorted(WordFreqTbl& frequencies, unsigned maxCount) -> vector<WordFreq> {
    auto byFreqDesc = [](WordFreq const& lhs, WordFreq const& rhs) { return lhs.second > rhs.second; };
    auto sortable = vector<WordFreq>{begin(frequencies), end(frequencies)};
    sort(begin(sortable), end(sortable), byFreqDesc);
    sortable.erase(sortable.begin() + maxCount, sortable.end());
    sortable.shrink_to_fit();
    return sortable;
}

string randColor() {
    auto color = uniform_int_distribution<unsigned>{0, 255};
    auto buf = ostringstream{};
    buf << hex << color(r) << color(r) << color(r);
    return buf.str();
}

auto mkTags(vector<WordFreq> words, float minFont, float maxFont) -> vector<string> {
    auto scale = (maxFont - minFont) / (words.front().second - words.back().second);

    auto tags = vector<string>{};
    transform(begin(words), end(words), back_inserter(tags), [scale, minFont](WordFreq& wf) {
        auto [word, count] = wf;
        char buf[500];
        sprintf(buf, "<span style='font-size: %dpx; color: #%s;'>%s</span>",
                static_cast<unsigned>(count * scale + minFont),
                randColor().c_str(),
                word.c_str()
        );
        return string{buf};
    });
    shuffle(begin(tags), end(tags), r);
    return tags;
}


void store(string filename, const vector<string>& tags) {
    auto outfile = ofstream{filename};
    if (!outfile) throw invalid_argument{"cannot open "s + filename};

    outfile << R"(
        <html>
        <head>
            <title>Word Frequencies</title>
            <style>
                body {font-family: sans-serif;}
                h1   {text-align: center; color: orange;}
            </style>
        </head>
        <body>
          <h1>Word Frequencies</h1>
    )";
    for (auto const& tag: tags) outfile << tag << endl;
    outfile << R"(
        </body>
        </html>
    )";
}
