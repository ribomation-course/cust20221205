#pragma once
#include <iostream>
#include <sstream>
#include <cstring>

namespace ribomation {
    using std::cout;

    inline char* copystr(const char* str) {
        if (str == nullptr) return nullptr;
        return strcpy(new char[strlen(str) + 1], str);
    }

    class Person {
        char* name = nullptr;
        unsigned age = 0;

    public:
        ~Person() {
            cout << "~Person @ " << this << "\n";
            delete[] name;
        }

        Person() {
            cout << "Person{} @ " << this << "\n";
            name = copystr("");
        }

        Person(const char* n, unsigned a)
                : name{copystr(n)}, age{a} {
            cout << "Person{const char* " << n << ", " << a << "} @ " << this << "\n";
        }

        Person(const Person& that)
                : name{copystr(that.name)}, age{that.age} {
            cout << "Person{const Person& " << &that << "} @ " << this << "\n";
        }

        Person(Person&& that) noexcept
                : name{that.name}, age{that.age} {
            that.name = nullptr;
            that.age  = 0;
            cout << "Person{Person&& " << &that << "} @ " << this << "\n";
        }

        auto operator=(const Person& that) -> Person& {
            cout << "operator =(const Person& " << &that << ") @ " << this << "\n";
            if (this != &that) {
                delete[] name;
                name = copystr(that.name);
                age  = that.age;
            }
            return *this;
        }

        auto operator=(Person&& that) noexcept -> Person& {
            cout << "operator =(Person&& " << &that << ") @ " << this << "\n";
            if (this != &that) {
                delete[] name;
                name = that.name;
                that.name = nullptr;
                age = that.age;
                that.age = 0;
            }
            return *this;
        }

        std::string toString() const {
            std::ostringstream buf{};
            buf << "Person(" << (name ? name : "??") << ", " << age << ") @ " << this;
            return buf.str();
        }

        unsigned incrAge() { return ++age; }

    };

}