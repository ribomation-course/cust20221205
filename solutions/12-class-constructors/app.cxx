#include <iostream>
#include <vector>
#include "person.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation;

auto func(Person q) -> Person {
    cout << "[func] q: " << q.toString() << endl;
    q.incrAge();
    cout << "[func] q: " << q.toString() << endl;
    return q;
}

int main() {
    cout << "[main] enter\n";
    {
        cout << "[block] enter\n";
        auto p = Person{"Cris P. Bacon", 27};
        cout << "[block] p: " << p.toString() << endl;

        cout << "[block] func(p)\n";
        auto q = func(p);
        cout << "[main] q: " << q.toString() << endl;

        cout << "[block] p = q\n";
        p = q;
        cout << "[block] p: " << p.toString() << endl;

        cout << "[block] p = std::move(q)\n";
        p = std::move(q);
        cout << "[block] p: " << p.toString() << endl;
        cout << "[block] q: " << q.toString() << endl;

        cout << "[block] exit\n";
    }

    cout << "[main] -- vector block before --\n";
    {
        cout << "[block2] enter\n";
        auto v = vector<Person>{
            {"Anna",  27}, {"Berit", 37}, {"Carin", 47}
        };

        cout << "[block2] for (auto p : v)\n";
        for (auto p : v) cout << "[cpy] p: " << p.toString() << endl;

        cout << "[block2] for (auto const& p : v)\n";
        for (auto const& p : v) cout <<"[ref]  p: "<< p.toString() << endl;

        cout << "[block2] exit\n";
    }
    cout << "[main] exit\n";
}
