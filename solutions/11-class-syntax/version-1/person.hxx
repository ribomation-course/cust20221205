#pragma once

#include <string>

namespace ribomation::domain {
    using std::string;

    class Person {
        string name;
        unsigned age;

    public:
        Person(string const& name, unsigned age);
        ~Person();

        string getName() const;
        void setName(string const& name);

        unsigned getAge() const;
        void setAge(unsigned age);

        string toString() const;
    };

}
