#include <iostream>
#include "person.hxx"

using std::cout;
using namespace std::string_literals;
using ribomation::domain::Person;

auto global = Person{"Anna Conda"s, 42};

int main() {
    cout << "[main] enter\n";
    cout << "global: " << global.toString() << "\n";
    {
        auto local = Person{"Per Silja"s, 37};
        cout << "local: " << local.toString() << "\n";

        auto heap = new Person{"Inge Vidare"s, 51};
        cout << "heap: " << heap->toString() << "\n";
        cout << "heap: " << (*heap).toString() << "\n";

        local.setName("Justin Time"s);
        local.setAge(local.getAge() + 3);
        cout << "local: " << local.toString() << "\n";

        delete heap;
    }
    cout << "[main] exit\n";
}
