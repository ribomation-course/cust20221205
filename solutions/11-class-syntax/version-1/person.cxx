#include <iostream>
#include "person.hxx"

namespace ribomation::domain {
    using namespace std::string_literals;
    using std::cout;

    Person::Person(const string& name_, unsigned age_)
            : name{name_}, age{age_} {
        cout << "Person{" << name << "} @ " << this << "\n";
    }

    Person::~Person() {
        cout << "~Person{" << name << "} @ " << this << "\n";
    }

    string Person::getName() const { return name; }
    void Person::setName(const string& name_) { name = name_; }

    unsigned Person::getAge() const { return age; }
    void Person::setAge(unsigned age_) { age = age_; }

    string Person::toString() const {
        return "Person{"s + name
               + ", "s + std::to_string(age)
               + "}"s;
    }
}
