#pragma once

#include <string>
#include <sstream>

namespace ribomation::domain {
    using std::string;
    using namespace std::string_literals;
    using std::cout;
    using std::ostringstream;


    class Person {
        string name;
        unsigned age;

    public:
        Person(string const& name_, unsigned age_) : name{name_}, age{age_} {
            cout << "Person{" << name << "} @ " << this << "\n";
        }

        ~Person() {
            cout << "~Person{" << name << "} @ " << this << "\n";
        }

        string getName() const { return name; }
        void setName(string const& name_) { name = name_; }

        unsigned getAge() const { return age; };
        void setAge(unsigned age_) { age = age_; }

        string toString() const {
            auto buf = ostringstream{};
            buf << "Person{" << name << ", " << age << "}";
            return buf.str();
        }
    };

}
