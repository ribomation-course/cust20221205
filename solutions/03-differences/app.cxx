#include <iostream>
#include <string>
#include "numbers.hxx"
using std::cout;

int main(int argc, char* argv[]) {
    auto n = argc==1 ? 10 : std::stoi(argv[1]);
    cout << "sum(" << n << ") = " << sum(n) << "\n";
    cout << "prod(" << n << ") = " << prod(n) << "\n";
}
