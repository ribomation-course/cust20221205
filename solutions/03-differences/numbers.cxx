
int sum(int n) {
    if (n <= 1) return n;
    return n + sum(n - 1);
}

int prod(int n) {
    if (n <= 1) return n;
    return n * prod(n - 1);
}
