#include <iostream>
#include <cassert>
#include "string-utils.hxx"

using namespace std;
using namespace std::string_literals;

void test_lowercase() {
    using ribomation::string_utils::lowercase;
    assert(lowercase("aBCd42"s) == "abcd42"s);
}

void test_uppercase() {
    using ribomation::string_utils::uppercase;
    assert(uppercase("aBCd42"s) == "ABCD42"s);
}

void test_strip() {
    using ribomation::string_utils::strip;
    assert(strip("aBCd42 *= hepp"s) == "aBCdhepp"s);
}

void test_truncate() {
    using ribomation::string_utils::truncate;
    assert(truncate("abc"s, 6) == "abc"s);
    assert(truncate("abcdef"s, 3) == "abc"s);
}

int main() {
    test_lowercase();
    test_uppercase();
    test_strip();
    test_truncate();
    cout << "all tests passed!\n";
    return 0;
}
