#pragma once

#include <iosfwd>
#include <sstream>
#include <string>
#include <utility>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Person {
        string          name;
        unsigned        age;
        static unsigned numPersons;

    public:
        Person(string name, unsigned age = 0)
                : name{std::move(name)}, age{age} {
            ++numPersons;
            cout << "CREATE: " << toString() << endl;
        }

        Person(const Person& other)
                : name{other.name}, age{other.age} {
            ++numPersons;
            cout << "COPY: " << toString() << endl;
        }

        ~Person() {
            --numPersons;
            cout << "DISPOSE: " << toString() << endl;
        }

        static unsigned count() { return numPersons; }

        string toString() const {
            ostringstream buf{};
            buf << "Person{" << name << ", " << age << "} (" << numPersons << ") @ " << this;
            return buf.str();
        }

        friend ostream& operator <<(ostream& os, const Person& p) {
            return os << p.toString();
        }
    };

}

