#include <iostream>
#include <vector>
#include <random>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

unsigned Person::numPersons = 0;
random_device r;
vector<string> names{
        "Anna"s, "Berit"s, "Carin"s, "Doris"s, "Eva"s, "Frida"s, "Greta"s
};
uniform_int_distribution<unsigned long> nextNameIndex{0, names.size() - 1};
uniform_int_distribution<unsigned> nextAge{20, 80};

auto mkPerson() -> Person {
    return {names[nextNameIndex(r)], nextAge(r)};
}

auto mkPersonPointer() -> Person* {
    return new Person{names[nextNameIndex(r)], nextAge(r)};
}


int main() {
    auto const N = 5;
    {
        cout << "--- vector<Person*> ---\n";
        auto persons = vector<Person*>{};
        for (auto k = 0; k < N; ++k) persons.push_back(mkPersonPointer());
        for (auto& p: persons) cout << "p: " << *p << endl;
        cout << "# persons = " << Person::count() << endl;
        while (!persons.empty()) {
            auto p = persons.back();
            persons.pop_back();
            delete p;
        }
    }
    cout << "# persons = " << Person::count() << endl;

    {
        cout << "--- vector<Person> push_back() ---\n";
        auto persons = vector<Person>{};
        for (auto k = 0; k < N; ++k) persons.push_back(mkPerson());
        for (auto /*const&*/ p: persons) cout << "p: " << p << endl;
    }
    cout << "# persons = " << Person::count() << endl;

    {
        cout << "--- Person arr[] ---\n";
        Person arr[] = {
                {"Anna"s, 20},{"Berit"s, 30},{"Carin"s, 30},{"Doris"s, 40}
        };
        for (auto /*const&*/ p: arr) cout << "p: " << p << endl;
    }
    cout << "# persons = " << Person::count() << endl;

    {
        cout << "--- vector<Person> emplace_back() ---\n";
        auto persons = vector<Person>{};
        persons.reserve(N);
        for (auto k = 0; k < N; ++k) {
            // new (addr in heap block) Person{args...}
            persons.emplace_back(names[nextNameIndex(r)], nextAge(r));
        }
        for (auto const& p: persons) cout << "p: " << p << endl;
    }
    cout << "# persons = " << Person::count() << endl;

    return 0;
}
