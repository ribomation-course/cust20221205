#pragma once
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

class Shape {
    string type;

protected:
    Shape(const string& type) : type(type) {
        cout << "Shape{" << getType() << "} @ " << this << endl;
    }

public:
    virtual ~Shape() {
        cout << "~Shape() type=" << getType() << " @ " << this << endl;
    }

    virtual const string& getType() const final { return type; }

    virtual double area() const = 0;

    virtual string toString() const {
        ostringstream buf;
        buf << getType() << " area=" << area() << " ";
        return buf.str();
    }
};

inline ostream& operator<<(ostream& os, const Shape& s) {
    return os << s.toString();
}

inline ostream& operator<<(ostream& os, const Shape* s) {
    return os << s->toString();
}

