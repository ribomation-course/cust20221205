#include <iostream>

using std::cout;

int&& fn(int a, int &b, int &&c, int &&d) {
    a *= 10;
    b *= 20;
    c *= 30;
    d *= 40;
    return std::move(c);
}

int main() {
    int number = 42;
    cout << "[before] number=" << number << "\n";
    auto x = fn(number, number, 5 * number, std::move(number));
    cout << "[after]  number=" << number << "\n";
    cout << "[after]  x=" << x << "\n";
}
