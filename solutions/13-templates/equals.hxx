#pragma once

template<typename Type>
bool equals(Type a, Type b, Type c) {
    return (a == b) && (b == c);
}

