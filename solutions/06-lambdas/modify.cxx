#include <iostream>
#include <algorithm>
#include <functional>

using namespace std;

void modify(int *arr, int size, function<int(int)> fn) {
    for (auto k = 0; k < size; ++k) {
        arr[k] = fn(arr[k]);
    }
}

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);

    auto print = [](auto n) { cout << n << " "; };
    for_each(numbers, numbers + N, print);
    cout << "\n";

    modify(numbers, N, [](int k) { return k * k; });

    for_each(numbers, numbers + N, print);
    cout << "\n";
}
