#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);

    auto print = [](auto n){ cout << n << " "; };
    for_each(numbers, numbers + N, print); cout<<"\n";

    auto factor = 42;
    auto fn = [factor](auto n){ return factor * n; };
    transform(numbers, numbers + N, numbers, fn);

    for_each(numbers, numbers + N, print); cout<<"\n";
}
