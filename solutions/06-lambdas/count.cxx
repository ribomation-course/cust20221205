#include <iostream>
#include <algorithm>
#include <functional>

using namespace std;

int count_if(int *first, int *last, function<bool(int)> is_true) {
    auto result = 0;
    for (; first != last; ++first) if (is_true(*first)) ++result;
    return result;
}

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    auto const N = sizeof(numbers) / sizeof(numbers[0]);

    auto is_even = [](auto k) { return k % 2 == 0; };
    auto cnt = count_if(numbers, numbers + N, is_even);
    cout << "# even: " << cnt << "\n";
}
