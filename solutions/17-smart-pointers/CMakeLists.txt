cmake_minimum_required(VERSION 3.14)
project(usage_of_smart_pointers)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(smart-pointers
        person.hxx
        app.cxx person.cxx)

