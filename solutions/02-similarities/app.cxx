#include <iostream>
#include <limits>

using std::cout;
using std::cin;
using std::numeric_limits;

int main() {
    auto count = 0U;
    auto sum = 0.0;
    auto minVal = numeric_limits<int>::max();
    auto maxVal = numeric_limits<int>::min();

    for (auto value = 0; true;) {
        cout << "Number? ";
        cin >> value;
        if (value == 0) break;
        ++count;
        sum += value;
        minVal = value < minVal ? value : minVal;
        maxVal = value > maxVal ? value : maxVal;
    }

    cout << "# values: " << count << "\n";
    cout << "average: " << (sum / count) << "\n";
    cout << "min/max: " << minVal << "/" << maxVal << "\n";
}
