#pragma once

#include <iostream>
#include <type_traits>
#include <cctype>

namespace ribomation {
    using std::is_arithmetic_v;
    using std::ostream;
    using std::istream;

    template<typename T = int>
    class Vector3d {
        T x = {};
        T y = {};
        T z = {};
        static_assert(is_arithmetic_v<T>, "Type parameter must be numeric");
    public:
        Vector3d() = default;
        Vector3d(T x, T y, T z) : x{x}, y{y}, z{z} {}
        T getX() const { return x; }
        T getY() const { return y; }
        T getZ() const { return z; }
    };

    template<typename T=int>
    ostream& operator <<(ostream& os, const Vector3d<T>& v) {
        return os << "[" << v.getX() << ", " << v.getY() << ", " << v.getZ() << "]";
    }

    template<typename T=int>
    istream& operator >>(istream& is, Vector3d<T>& vec) {
        {
            //find the first digit, and parse from there
            char ch;
            while (is.get(ch) && !isdigit(ch));
            is.putback(ch);
        }
        {
            T x, y, z;
            is >> x >> y >> z;
            vec = Vector3d<T>{x, y, z};
        }
        return is;
    }

    template<typename T=int>
    Vector3d<T> operator *(const Vector3d<T>& v, T factor) {
        return {v.getX() * factor,
                v.getY() * factor,
                v.getZ() * factor};
    }

    template<typename T=int>
    Vector3d<T> operator *(T factor, const Vector3d<T>& v) {
        return v * factor;
    }

    template<typename T=int>
    Vector3d<T> operator +(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return {lhs.getX() + rhs.getX(),
                lhs.getY() + rhs.getY(),
                lhs.getZ() + rhs.getZ()};
    }

    template<typename T=int>
    Vector3d<T> operator -(const Vector3d<T>& vec) {
        return {-vec.getX(), -vec.getY(), -vec.getZ()};
    }

    template<typename T=int>
    Vector3d<T> operator -(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return lhs + -rhs;
    }

    template<typename T=int>
    T operator *(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return lhs.getX() * rhs.getX()
               + lhs.getY() * rhs.getY()
               + lhs.getZ() * rhs.getZ();
    }

}
