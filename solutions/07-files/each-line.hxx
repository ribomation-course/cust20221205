#include <iostream>
#include <fstream>
#include <stdexcept>
#include <functional>
#include <string>
#include <utility>

namespace ribomation::io {
    using std::istream;
    using std::ifstream;
    using std::getline;
    using std::invalid_argument;
    using std::function;
    using std::string;

    using ProcessLineFn = function<void (string const&)>;

    void each_line(istream& is, ProcessLineFn const& processLine) {
        for (auto line = string{}; getline(is, line);) {
            processLine(line);
        }
    }

    void each_line(string const& filename, ProcessLineFn const& processLine) {
        auto file = ifstream{filename};
        if (!file) throw invalid_argument{"cannot open " + filename};
        each_line(file, processLine);
    }
}
