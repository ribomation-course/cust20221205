#include <iostream>
#include <sstream>
#include <iomanip>
#include "each-line.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::io;

struct Counts {
private:
    unsigned lines = 0;
    unsigned words = 0;
    unsigned chars = 0;
    friend void operator+=(Counts& lhs, Counts const &rhs);
};

void operator+=(Counts& lhs, Counts const &rhs) {
    lhs.lines += rhs.lines;
    lhs.words += rhs.words;
    lhs.chars += rhs.chars;
}

auto operator<<(ostream &os, Counts const &cnt) -> ostream & {
    return os << setw(4) << cnt.lines << " "
              << setw(6) << cnt.words << " "
              << setw(8) << cnt.chars;
}

/*
 cout << obj --> cout
 operator <<(cout, obj) --> cout

 cout << "obj=" << obj << "\n";
 operator <<( operator <<(cout, "obj="), obj)

 * */

auto count(istream &in) -> Counts {
    auto countWords = [](string const &line) -> unsigned {
        auto cnt = 0U;
        auto buf = istringstream{line};
        for (string word; buf >> word;) ++cnt;
        return cnt;
    };

    auto cnt = Counts{};
    each_line(in, [&cnt, &countWords](auto const &line) {
        ++cnt.lines;
        cnt.words += countWords(line);
        cnt.chars += line.size() + 1;
    });

    return cnt;
}

int main(int argc, char **argv) {
    if (argc <= 1) {
        auto stdin = count(cin);
        cout << stdin << endl;
    } else {
        auto total = Counts{};
        for (auto k = 1U; k < argc; ++k) {
            auto filename = string{argv[k]};
            auto file = ifstream{filename};
            auto cnt = count(file);
            cout << cnt << " " << filename << endl;
            total += cnt;
        }
        if (argc > 2) {
            cout << total << " total" << endl;
        }
    }
    return 0;
}
