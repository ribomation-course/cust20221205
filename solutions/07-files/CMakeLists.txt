cmake_minimum_required(VERSION 3.16)
project(07_files)

set(CMAKE_CXX_STANDARD 17)

add_executable(each-line-test
        each-line.hxx
        each-line-test.cxx
)

add_executable(word-count
        each-line.hxx
        word-count.cxx
)


