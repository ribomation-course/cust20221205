#include <iostream>
#include "each-line.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::io;

int main(int argc, char **argv) {
    string filename = (argc == 1) ? "../each-line-test.cxx"s : argv[1];
    auto lineno = 1U;
    each_line(filename, [&lineno](auto const &line) {
        cout << "[" << lineno++ << "] " << line << endl;
    });
}
