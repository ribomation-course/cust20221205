#include <iostream>
#include "trace-alloc.hxx"

using namespace std;

int main() {
    {
        cout << "-- single --\n";
        auto mgr = AllocManager{};
        auto ptr = new long double{42};
        cout << "*ptr=" << *ptr << " (" << ptr << ")\n";
        delete ptr;
    }

    {
        cout << "-- array --\n";
        auto       mgr = AllocManager{};
        auto const N   = 10;
        auto       arr = new int[N];

        for (auto k = 0; k < N; ++k)
            arr[k] = k < 2 ? 1 : arr[k - 2] + arr[k - 1];

        for (auto k = 0; k < N; ++k)
            cout << "arr[" << k << "] = " << arr[k] << endl;

        delete[] arr;
    }

    return 0;
}


