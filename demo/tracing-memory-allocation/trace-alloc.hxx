#pragma once

#include <new>

auto operator new(size_t numBytes) -> void*;
auto operator new[](size_t numBytes) -> void*;

void operator delete(void* addr) noexcept;
void operator delete(void*, std::size_t) noexcept;
void operator delete[](void* addr) noexcept;
void operator delete[](void*, std::size_t) noexcept;

void printStats();
void resetStats();

struct AllocManager {
    AllocManager() { resetStats(); }
    ~AllocManager() { printStats(); }
};

