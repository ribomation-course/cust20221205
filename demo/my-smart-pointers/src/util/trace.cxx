#include "trace.hxx"

#ifdef NDEBUG
struct NullStream : std::ostream {
    NullStream() : std::ostream{nullptr} {/*std::cerr<<"NullStream created\n";*/}

    NullStream(const NullStream&) : std::basic_ios<char>{}, std::ostream{nullptr} {}
};

auto devNull = /*ribomation::util::*/NullStream{};
std::ostream* ribomation::util::Trace::logstream = &devNull;

#else

std::ostream* ribomation::util::Trace::logstream = &cout;

#endif

int ribomation::util::Trace::level = 0;
